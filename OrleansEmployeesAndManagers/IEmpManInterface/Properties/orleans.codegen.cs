#if !EXCLUDE_CODEGEN
#pragma warning disable 162
#pragma warning disable 219
#pragma warning disable 414
#pragma warning disable 649
#pragma warning disable 693
#pragma warning disable 1591
#pragma warning disable 1998
[assembly: global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.0.0")]
[assembly: global::Orleans.CodeGeneration.OrleansCodeGenerationTargetAttribute("IEmpManInterface, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")]
namespace IEmpManInterface
{
    using global::Orleans.Async;
    using global::Orleans;
    using global::System.Reflection;

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IEmpManInterface.IEmployee))]
    internal class OrleansCodeGenEmployeeReference : global::Orleans.Runtime.GrainReference, global::IEmpManInterface.IEmployee
    {
        protected @OrleansCodeGenEmployeeReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenEmployeeReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -1254521027;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IEmpManInterface.IEmployee";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -1254521027;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -1254521027:
                    switch (@methodId)
                    {
                        case 312402437:
                            return "GetLevel";
                        case -1461035780:
                            return "Promote";
                        case 940043127:
                            return "GetManager";
                        case -159913581:
                            return "SetManager";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -1254521027 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::System.Int32> @GetLevel()
        {
            return base.@InvokeMethodAsync<global::System.Int32>(312402437, null);
        }

        public global::System.Threading.Tasks.Task @Promote(global::System.Int32 @newLevel)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-1461035780, new global::System.Object[]{@newLevel});
        }

        public global::System.Threading.Tasks.Task<global::IEmpManInterface.IManager> @GetManager()
        {
            return base.@InvokeMethodAsync<global::IEmpManInterface.IManager>(940043127, null);
        }

        public global::System.Threading.Tasks.Task @SetManager(global::IEmpManInterface.IManager @manager)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-159913581, new global::System.Object[]{@manager is global::Orleans.Grain ? @manager.@AsReference<global::IEmpManInterface.IManager>() : @manager});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::IEmpManInterface.IEmployee", -1254521027, typeof (global::IEmpManInterface.IEmployee)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenEmployeeMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            try
            {
                if (@grain == null)
                    throw new global::System.ArgumentNullException("grain");
                switch (interfaceId)
                {
                    case -1254521027:
                        switch (methodId)
                        {
                            case 312402437:
                                return ((global::IEmpManInterface.IEmployee)@grain).@GetLevel().@Box();
                            case -1461035780:
                                return ((global::IEmpManInterface.IEmployee)@grain).@Promote((global::System.Int32)arguments[0]).@Box();
                            case 940043127:
                                return ((global::IEmpManInterface.IEmployee)@grain).@GetManager().@Box();
                            case -159913581:
                                return ((global::IEmpManInterface.IEmployee)@grain).@SetManager((global::IEmpManInterface.IManager)arguments[0]).@Box();
                            default:
                                throw new global::System.NotImplementedException("interfaceId=" + -1254521027 + ",methodId=" + methodId);
                        }

                    default:
                        throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
                }
            }
            catch (global::System.Exception exception)
            {
                return global::Orleans.Async.TaskUtility.@Faulted(exception);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -1254521027;
            }
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.0.0"), global::System.SerializableAttribute, global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute, global::Orleans.CodeGeneration.GrainReferenceAttribute(typeof (global::IEmpManInterface.IManager))]
    internal class OrleansCodeGenManagerReference : global::Orleans.Runtime.GrainReference, global::IEmpManInterface.IManager
    {
        protected @OrleansCodeGenManagerReference(global::Orleans.Runtime.GrainReference @other): base (@other)
        {
        }

        protected @OrleansCodeGenManagerReference(global::System.Runtime.Serialization.SerializationInfo @info, global::System.Runtime.Serialization.StreamingContext @context): base (@info, @context)
        {
        }

        protected override global::System.Int32 InterfaceId
        {
            get
            {
                return -2032877289;
            }
        }

        public override global::System.String InterfaceName
        {
            get
            {
                return "global::IEmpManInterface.IManager";
            }
        }

        public override global::System.Boolean @IsCompatible(global::System.Int32 @interfaceId)
        {
            return @interfaceId == -2032877289;
        }

        protected override global::System.String @GetMethodName(global::System.Int32 @interfaceId, global::System.Int32 @methodId)
        {
            switch (@interfaceId)
            {
                case -2032877289:
                    switch (@methodId)
                    {
                        case -1701259070:
                            return "AsEmployee";
                        case -675836216:
                            return "GetDirectReports";
                        case -1731443267:
                            return "AddDirectReport";
                        default:
                            throw new global::System.NotImplementedException("interfaceId=" + -2032877289 + ",methodId=" + @methodId);
                    }

                default:
                    throw new global::System.NotImplementedException("interfaceId=" + @interfaceId);
            }
        }

        public global::System.Threading.Tasks.Task<global::IEmpManInterface.IEmployee> @AsEmployee()
        {
            return base.@InvokeMethodAsync<global::IEmpManInterface.IEmployee>(-1701259070, null);
        }

        public global::System.Threading.Tasks.Task<global::System.Collections.Generic.List<global::IEmpManInterface.IEmployee>> @GetDirectReports()
        {
            return base.@InvokeMethodAsync<global::System.Collections.Generic.List<global::IEmpManInterface.IEmployee>>(-675836216, null);
        }

        public global::System.Threading.Tasks.Task @AddDirectReport(global::IEmpManInterface.IEmployee @employee)
        {
            return base.@InvokeMethodAsync<global::System.Object>(-1731443267, new global::System.Object[]{@employee is global::Orleans.Grain ? @employee.@AsReference<global::IEmpManInterface.IEmployee>() : @employee});
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Orleans-CodeGenerator", "1.3.0.0"), global::Orleans.CodeGeneration.MethodInvokerAttribute("global::IEmpManInterface.IManager", -2032877289, typeof (global::IEmpManInterface.IManager)), global::System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverageAttribute]
    internal class OrleansCodeGenManagerMethodInvoker : global::Orleans.CodeGeneration.IGrainMethodInvoker
    {
        public global::System.Threading.Tasks.Task<global::System.Object> @Invoke(global::Orleans.Runtime.IAddressable @grain, global::Orleans.CodeGeneration.InvokeMethodRequest @request)
        {
            global::System.Int32 interfaceId = @request.@InterfaceId;
            global::System.Int32 methodId = @request.@MethodId;
            global::System.Object[] arguments = @request.@Arguments;
            try
            {
                if (@grain == null)
                    throw new global::System.ArgumentNullException("grain");
                switch (interfaceId)
                {
                    case -2032877289:
                        switch (methodId)
                        {
                            case -1701259070:
                                return ((global::IEmpManInterface.IManager)@grain).@AsEmployee().@Box();
                            case -675836216:
                                return ((global::IEmpManInterface.IManager)@grain).@GetDirectReports().@Box();
                            case -1731443267:
                                return ((global::IEmpManInterface.IManager)@grain).@AddDirectReport((global::IEmpManInterface.IEmployee)arguments[0]).@Box();
                            default:
                                throw new global::System.NotImplementedException("interfaceId=" + -2032877289 + ",methodId=" + methodId);
                        }

                    default:
                        throw new global::System.NotImplementedException("interfaceId=" + interfaceId);
                }
            }
            catch (global::System.Exception exception)
            {
                return global::Orleans.Async.TaskUtility.@Faulted(exception);
            }
        }

        public global::System.Int32 InterfaceId
        {
            get
            {
                return -2032877289;
            }
        }
    }
}
#pragma warning restore 162
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 649
#pragma warning restore 693
#pragma warning restore 1591
#pragma warning restore 1998
#endif
