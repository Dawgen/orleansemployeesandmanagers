using System.Threading.Tasks;
using Orleans;
using System.Collections.Generic;

namespace IEmpManInterface
{
    /// <summary>
    /// Grain interface IGrain1
    /// </summary>
	public interface IEmployee : IGrainWithGuidKey
    {
        Task<int> GetLevel();
        Task Promote(int newLevel);

        Task<IManager> GetManager();
        Task SetManager(IManager manager);
    }

    public interface IManager : IGrainWithGuidKey
    {
        Task<IEmployee> AsEmployee();
        Task<List<IEmployee>> GetDirectReports();
        Task AddDirectReport(IEmployee employee);
    }
}
