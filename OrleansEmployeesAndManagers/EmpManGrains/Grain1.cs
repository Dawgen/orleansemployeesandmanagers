using System.Threading.Tasks;
using Orleans;
using IEmpManInterface;
using System;

namespace EmpManGrains
{
    /// <summary>
    /// Grain implementation class Grain1.
    /// </summary>
    public class Grain1 : Grain, IEmployee
    {
        public Task<int> GetLevel()
        {
            throw new NotImplementedException();
        }

        public Task<IManager> GetManager()
        {
            throw new NotImplementedException();
        }

        public Task Promote(int newLevel)
        {
            throw new NotImplementedException();
        }

        public Task SetManager(IManager manager)
        {
            throw new NotImplementedException();
        }
    }
}
